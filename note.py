# -*- coding: utf-8 -*-
from robobrowser import RoboBrowser
from getpass import getpass
from re import sub
from argparse import ArgumentParser

def print_as_table(ls):
    print()
    row_format ="{:<45}{:<8}{:<8}"
    print(row_format.format("name", "grade", "LP"))
    for n, g, l in ls:
        print(row_format.format(n, g, l))
    print()


if __name__ == "__main__":

    args = ArgumentParser(description="This program calculates the relevant grades and their mean.")
    args.add_argument('--output', type=str, help='specify output location')
    # args.add_argument('--format', type=str, help='save to format (txt | pdf)')
    args.add_argument('--add', type=str, help='add following modules to the calculations. Format "name, grade, lp"')
    args, leftovers = args.parse_known_args()

    additional = []
    if args.add is not None:
        parts = args.add.split(',')
        additional.append((parts[0], float(parts[1]), int(parts[2])))

    username = input("Username: ")
    password = getpass()

    print("connecting and browsing campusonline...")
    BASE_URL = "https://campusonline.uni-ulm.de/qislsf/rds?state=user&type=0"
    browser = RoboBrowser(parser="html.parser")
    browser.open(BASE_URL)

    # as the website does not supply a single link per resource and handles user data with cookies, 
    # but integrates the user into the URL somehow, we need to follow some links to get to the grade file

    login_form = browser.get_forms()[0]
    login_form['asdf'] = username       # no semantic html tags :(
    login_form['fdsa'] = password
    browser.submit_form(login_form)

    # The site always returns 200, no matter if login was successful or not
    # assert browser.response.status_code == 200, "Wrong Username or password!"

    # do not keep user info longer in RAM than necessary
    username = password = None

    link = browser.find('a', text="Prüfungsverwaltung")
    try:
        browser.follow_link(link)
    except TypeError:
        print("Could not find the expected link on the Homepage. This means that either the site was redesigned ("
              "unlikely) or your credentials were wrong (likely).")
        exit(1)

    link = browser.find('a', text="HTML-Ansicht Ihrer erbrachten Leistungen")
    browser.follow_link(link)

    link = browser.find('a', text="Abschluss Bachelor of Science")
    browser.follow_link(link)

    link = browser.find('a', title="Leistungen für Informatik  anzeigen")
    browser.follow_link(link)

    table_html = browser.find_all('table')[1]

    table = [[sub(r" +", " ", sub(r"[\r\n\t]", "", elem.text)) for elem in line.find_all('td')]
             for line in table_html.find_all('tr')]

    table = [[x for x in line] for line in table if line != [] and int(line[0]) > 9999
             and len(line) == 9 and line[8] != '']
    table = [x for x in table if 'UB' != x[6]]

    modules = [(e[1], float(e[3].replace(',', '.')), int(e[5])) for e in table]

    modules += additional

    modules = sorted(modules, key=lambda x: (x[1], x[2]))
    lp = 0
    rel = []
    while lp < 90:
        rel.append(modules.pop(0))
        lp += int(rel[-1][2])

    print("following modules are relevant:")
    print_as_table(rel)
    print("sum of %i lp" % lp)
    avg = sum(grade * lp for (name, grade, lp) in rel) / lp
    print("avg: {:1.2f}".format(avg))
    print("bachelor limit: {:1.2f}".format(min((2.7 * (lp + 12) - avg * lp) / 12, 4.0)))
